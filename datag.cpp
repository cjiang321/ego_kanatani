#include "datag.h"
#include "utilsf.h"
#include <sstream>

//#define FLOW_DEBUG
const int MAX_COUNT = 100;

datag::datag(const cv::Mat_<float> cameraMatrix)
{
    K = cameraMatrix;
    counter = 0;
}

void datag::inputFrames(const cv::Mat &frame1, const cv::Mat &frame2)
{
    f1 = frame1;
    f2 = frame2;

    flow.clear();
    pts.clear();
}

void datag::outputData(std::vector< cv::Point2f > &out_pts, std::vector< cv::Point2f > &out_flow)
{

    //scaling point set and flow vector
    for (size_t i = 0; i < pts.size(); i++)
    {
        flow[i].x = (flow[i].x - pts[i].x) / K(0,0);
        flow[i].y = (flow[i].y - pts[i].y) / K(1,1);
        pts[i].x -= (K(0,2));
        pts[i].x = pts[i].x / K(0,0);
        pts[i].y -= (K(1,2));
        pts[i].y = pts[i].y / K(1,1);
    }

// The following code has been incorperated into the above loop
//    for (size_t i = 0; i < pts.size(); i++)
//    {
//        pts[i].x = pts[i].x / K(0,0);
//        pts[i].y = pts[i].y / K(1,1);
//        flow[i].x /= K(0,0);
//        flow[i].y /= K(1,1);
//    }

    //output
    out_pts = pts;
    out_flow = flow;

    std::cout << "number of points remaining are: " << pts.size() << std::endl;
}

/*
void datag::calcFlow(void)
{
    //detect fast features to obtian point set
    std::vector< cv::KeyPoint > ori_Keypts;

    cv::FastFeatureDetector ffd;
    ffd.detect(f1, ori_Keypts);

    //calculate flow for point set
    std::vector< cv::Point2f > ori_pts, next_pts;
    std::vector< uchar > vstatus;
    std::vector< float > verror;

    for (size_t i = 0; i < ori_Keypts.size(); i++)
        ori_pts.push_back(ori_Keypts[i].pt);

    std::cout << "total number of feature points are: " << ori_pts.size() << std::endl;

    cv::calcOpticalFlowPyrLK(f1, f2, ori_pts, next_pts, vstatus, verror, cv::Size(50, 50), 5);

    std::cout << "opticla flow computed" << std::endl;

    for (size_t i = 0; i < ori_pts.size(); i++)
    {
        if (vstatus[i] && (verror[i] < 5.0))
        {
            pts.push_back(ori_pts[i]);
            flow.push_back(next_pts[i]);

            if (pts.size() >= 500)
                break;
        }
    }

    std::cout << "number of points remaining are: " << pts.size() << std::endl;

#ifdef FLOW_DEBUG
    //draw flow vectors
    if (pts.size() != flow.size())
        assert(0);
    cv::Mat drawf = f1.clone();
    for (size_t i = 0; i < flow.size(); i++)
    {
        drawArrows(drawf, pts[i], flow[i]);
    }

    cv::imshow("flow vector", drawf);
    counter ++;
    std::string filename = "/home/jiangchao/Documents/flow" + static_cast<std::ostringstream*>( &(std::ostringstream() << counter) )->str() + ".bmp";
    cv::imwrite(filename, drawf);

#endif
}
*/

void datag::calcFlow(void)
{
   cv::TermCriteria termcrit(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, 0.03);
   cv::Size subPixSize(10,10), winSize(31,31);

   //std::cout << "obtain features in the next image" << std::endl;

   cv::goodFeaturesToTrack(f1, points[0], MAX_COUNT, 0.01, 10, cv::Mat(), 3, 0, 0.04);
   //cv::cornerSubPix(f2, points[1], subPixSize, cv::Size(-1, -1), termcrit);

   //std::cout << "calcualting optical flow" << std::endl;

   std::vector< cv::Point2f > tmp;
   //for (size_t i = 0; i < points[1].size(); i++)
   //    tmp.push_back(points[1][i]);

   std::vector< uchar > vstatus;
   std::vector< float > verror;

   if (points[0].empty())
   {
       std::cout << "Error: points 0 empty" << std::endl;
       assert(0);
   }
   cv::calcOpticalFlowPyrLK(f1, f2, points[0], points[1], vstatus, verror, winSize, 3, termcrit, 0, 0.001);

   std::cout << "resizing points" << std::endl;
   std::cout << points[0].size() << std::endl << points[1].size() << std::endl;

   size_t i, j, k;
   for (i = j = k = 0; i < points[1].size(); i++)
   {
       if (!vstatus[i] || (verror[i] >= 5.0))
       {
           //tmp[j++] = tmp[i];
           continue;
       }
       points[0][k] = points[0][i];
       points[1][k++] = points[1][i];
   }
   std::cout << "k is " << k << "  j is: " << j << std::endl;
   points[0].resize(k);
   points[1].resize(k);
   //tmp.resize(j);

   for (i = 0; i < k; i++)
   {
       pts.push_back(points[0][i]);
       flow.push_back(points[1][i]);
   }

/*   std::cout << "adding points" << std::endl;

   //adding points
   if (points[1].size() < (size_t)MAX_COUNT)
   {
       cv::goodFeaturesToTrack(f2, tmp, abs(MAX_COUNT - k), 0.01, 10, cv::Mat(), 3, 0, 0.04);
       for (i = 0; i < tmp.size(); i++)
       {
           for (j = 0; j < k; j++)
           {
               if (norm(points[1][j] - tmp[i]) > 50)
               {
                   points[1].push_back(tmp[i]);
                   if (points[1].size() >= MAX_COUNT)
                       break;
               }
           }
       }
   }

   std::cout << k << std::endl;

   std::swap(points[1], points[0]);*/
/*
   std::cout << "drawing" << std::endl;

   //draw flow vectors
   if (pts.size() != flow.size())
       assert(0);
   cv::Mat drawf = f1.clone();
   for (size_t i = 0; i < flow.size(); i++)
   {
       drawArrows(drawf, pts[i], flow[i]);
   }

   cv::imshow("flow vector", drawf);
   cv::waitKey(20);
   //while (cv::waitKey(20) != ' ');

   counter ++;*/
   //std::string filename = "/home/jiangchao/Documents/flow" + static_cast<std::ostringstream*>( &(std::ostringstream() << counter) )->str() + ".bmp";
   //cv::imwrite(filename, drawf);
}

void datag::init_pt(void)
{
    cv::TermCriteria termcrit(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, 0.03);
    cv::Size subPixSize(10,10);

    cv::goodFeaturesToTrack(f1, points[0], MAX_COUNT, 0.01, 10, cv::Mat(), 3, 0, 0.04);
    //cv::cornerSubPix(f1, points[0], subPixSize, cv::Size(-1, -1), termcrit);
}
