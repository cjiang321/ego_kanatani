TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    ego.cpp \
    datag.cpp \
    utilsf.cpp \
    Client.cpp

LIBS += \
    `pkg-config opencv --libs`

HEADERS += \
    ego.h \
    datag.h \
    utilsf.h \
    Client.h

INCLUDEPATH += /usr/local/include/eigen3/

QMAKE_CXXFLAGS += -msse2
