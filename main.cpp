#include <iostream>
#include <sstream>
#include <fstream>

#include "ego.h"
#include "datag.h"
#include "Client.h"
#include "cvcapture_gstreamer.h"

using namespace std;

//#define EGO_DEBUG
//#define DEBUG
//t#define NETLOG

int main()
{
    //camera parameters
    //cv::Matx33d Kv(7.7361744223683297e+02, 0, 3.1458590038761173e+02, 0,
    //           7.7892826321280029e+02, 2.6059283493644153e+02, 0, 0, 1);   //gajen's cam @ 640x480
    //cv::Mat_<double> K = cv::Mat_<double>(Kv);

    cv::Matx33f Kv(5.8781933868326530e+02, 0, 3.3127983417951896e+02,
              0, 5.8910513442917556e+02, 2.3486662037824394e+02,
              0, 0, 1);
    cv::Mat_<float> K = cv::Mat_<float>(Kv);

    //float a[] = {1.1132964927611745e-02, 2.4454804016328133e-01,
    //            -6.1141627364070666e-03, 8.7006706565327641e-04,
    //            -1.3041149315466041e+00};
    //cv::Mat distortion = cv::Mat(5, 1, CV_32F, a);

    //variables
    cv::Point3f calib_v(10, 0, 0);

    vector< cv::Point2f > flow;
    vector< cv::Point2f > pts;

    float f = (K(0,0) + K(1,1)) / 2;

    cv::Point3f v,r;
    float depth;

    datag genData(K);

#ifdef NETLOG
    //initialise client for network data logging
    Client netcli;
#endif

    //initialise file for data logging
    ofstream fout;
    fout.open("output_log.txt");

    //initialise images

    //cout << "reading images..." << endl;

    cv::Mat img1 = cv::imread("/home/ubuntu/Documents/xysand/cap2.bmp", 0);
    cv::Mat img2 = cv::imread("/home/ubuntu/Documents/xysand/cap3.bmp", 0);
    //cv::Mat img1 = cv::imread("/home/jiangchao/Documents/orientation/cap1.bmp", 0);
    //cv::Mat img2 = cv::imread("/home/jiangchao/Documents/orientation/cap2.bmp", 0);

    //first computation of known movement to initialise height parameter
    //cout << "height calibration..." << endl;

    genData.inputFrames(img1, img2);
    genData.init_pt();
    genData.calcFlow();
    genData.outputData(pts, flow);

    ego egomotion(flow, pts, f);
    egomotion.kanatani(v, r, depth);

    float ratio = cv::norm(calib_v) / cv::norm(v);
    float realheight = fabs(depth) * ratio;

    //cout << "calibration done!" << endl;
    //cout << "###############################" << endl;

    //cache all video frames
    //cout << "caching video file..." << endl;

    CvCapture_GStreamer gst_cap;

    int type = CV_CAP_GSTREAMER_IMXV4L;
    //int type = CV_CAP_GSTREAMER_V4L2;

    if (!gst_cap.open(type, NULL))
    {
        cout << "fail open device with gstreamer" << endl;
        return -1;
    }

    for (int i = 0; i < 20; i++){
        gst_cap.grabFrame();
        cv::waitKey(100);
        gst_cap.retrieveFrame(0);
    }

    IplImage *iplim;

    double state;
    state = gst_cap.getProperty(CV_CAP_PROP_FRAME_HEIGHT);
    cout << "frame height is: " << state << endl;
    state = gst_cap.getProperty(CV_CAP_PROP_FRAME_WIDTH);
    cout << "frame width is:  " << state << endl;
    state = gst_cap.getProperty(CV_CAP_PROP_FPS);
    cout << "current fps is:  " << state << endl;
/*
    vector< cv::Mat > framelist;

    cv::VideoCapture cap;
    if (!cap.open("/home/jiangchao/Documents/xysand/test.avi"))
    {
        cout << "error opening video file" << endl;
        assert(0);
    }

    for (int i = 0; i < 814; i++)
    {
        cap >> img2;
        if (img2.empty())
        {
            cout << "error grabbing frame: " << i << endl;
            assert(0);
        }
        cv::cvtColor(img2, img2, CV_RGB2GRAY);
        framelist.push_back(img2);
        cv::waitKey(20);
    }
*/
    /*cout << "caching image files..." << endl;

    for (int i = 1; i <= 12; i++)
    {
        string filename = "/home/jiangchao/Documents/orientation/cap" + static_cast<ostringstream*>( &(ostringstream() << i) )->str() + ".bmp";
        img1 = cv::imread(filename, 0);
        if (img1.empty())
        {
            cout << "error reading image file: " << i << endl;
            assert(0);
        }
        framelist.push_back(img1);
        cv::waitKey(20);
    }*/

    //cout << "caching done" << endl;


    float time = cv::getTickCount();

    gst_cap.grabFrame();

    iplim = gst_cap.retrieveFrame(1);

    cv::cvtColor(cv::Mat(iplim), img2, CV_RGB2GRAY);
    cv::waitKey(20);

    int i = 0;
    //img2 = framelist[0];
    //for (i = 1; i < (int)framelist.size(); i++)
    for (;;)
    {
        img1 = img2.clone();

        //img2 = framelist[i];
        gst_cap.grabFrame();

        iplim = gst_cap.retrieveFrame(1);

        cv::cvtColor(cv::Mat(iplim), img2, CV_RGB2GRAY);

        if (img1.empty() || img2.empty())
        {
            cout << "error retrieving frame, or the end has reached" << endl;
            break;
        }

        //cv::imshow("video", img2);
        //cv::waitKey(20);

        //compute optical flow from them
        cout << "computing flow and generating data..." << endl;

        genData.inputFrames(img1, img2);
        genData.calcFlow();
        genData.outputData(pts, flow);

        //calculate translation and rotation velocity
        //cout << "computing egomotion..." << endl;

        egomotion = ego(flow, pts, f);
        egomotion.kanatani(v, r, depth);

        ratio = realheight / fabs(depth);
        cv::Point3f realv(v.x * ratio, v.y * ratio, v.z * ratio);

        //update height

        //cout << endl;
        //cout << depth << endl << ratio << endl << realheight << endl;

        if (depth < 0)
            realv = cv::Point3d(-realv.x, -realv.y, -realv.z);

        realheight += realv.z;

        //output
        cout << "############################" << endl;
        cout << i++ << endl;
        cout << "############################" << endl
             << "########## RESULT ##########" << endl
             << "############################" << endl;

        cout << "translation" << endl;
        cout << v << endl;
        cout << realv << endl;
        cout << "rotation" << endl;
        cout << r << endl;

        cout << "############################" << endl;

#ifdef NETLOG
        //output to network
        char sendMessage[30];
        sprintf(sendMessage, "#%02.2f;%02.2f;%02.2f\n", realv.x, realv.y, realv.z);
        sendMessage[strlen(sendMessage)] = '\0';
        netcli.ClientSend(sendMessage);
#endif

        //output to file
        fout << i << "\t" << realv.x << "\t" << realv.y << "\t" << realv.z << "\t";
        fout << r.x << "\t" << r.y << "\t" << r.z;
        if (fabs(realv.z) > 10)
        {
            fout << " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
        }
        fout << endl;

        //while (cv::waitKey(20) != ' ');
    }

    time = (cv::getTickCount() - time) / cv::getTickFrequency();

    cout << "total number of frames: " << i << endl;
    cout << "time elapsed: " << time << endl;
    cout << "freqency is: " << i / time << endl;

    //cap.release();
    gst_cap.close();
    fout.close();
#ifdef NETLOG
    netcli.ClientClose();
#endif

#ifdef DEBUG
    while (cv::waitKey(20) != ' ');
    cv::destroyAllWindows();
#endif


    return 0;
}

