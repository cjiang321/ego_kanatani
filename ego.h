#ifndef EGO_H
#define EGO_H

#include <opencv2/opencv.hpp>

class ego
{
private:
    std::vector< cv::Point2f > flow;    // vector containing optical flow
    std::vector< float > xp;            // x coordinate of corresponding flow
    std::vector< float > yp;            // y coordiante of corresponding flow
    cv::Point3f v;                      // translational velocity
    cv::Point3f w;                      // rotational velocity
    float focalLength;

public:

    ego(std::vector< cv::Point2f > &inflow,
        std::vector< cv::Point2f > &inpts,
        float infl);

    int kanatani(cv::Point3f &vtrans, cv::Point3f &vrot, float &depth_median);

};

#endif // EGO_H
