// Transmission Control Protocol (TCP) HEADER file
#ifndef Client_H
#define Client_H


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>



// Define the size of the SEND buffer.
#define DEFAULT_BUFFER_LENGTH 512
// Define the port number associated with the server to which the client will connect to.
#define DEFAULT_PORT "50"
// Define the IP Addresses of the host on which the server is running.
#define DEFAULT_HOST "10.0.0.1"
// Define DEBUG mode.
//#define DEBUG


using namespace std;


class Client {

private:
	int connectSocket, 
	    status, 
	    msgerror;
        struct addrinfo serverAddress;
        struct addrinfo *server;
	char sendBuffer[DEFAULT_BUFFER_LENGTH];

public:
	Client();
	void ClientSend(char* sendMessage);
	void ClientClose();

};

#endif
