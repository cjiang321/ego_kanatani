#include "Client.h"

void ErrorMessage(const char *msg) {

    perror(msg);
    exit(0);
}



Client::Client() {
 	
	printf("Initialising ...\n");


    memset(&serverAddress, 0, sizeof serverAddress);
    memset(&server, 0, sizeof server);
    serverAddress.ai_family = AF_UNSPEC;     // IP version not specified. Can be both.
    serverAddress.ai_socktype = SOCK_STREAM;


	printf("Getting address information ...\n");
    status = getaddrinfo(DEFAULT_HOST, DEFAULT_PORT, &serverAddress, &server);
    if (status != 0) {
        ErrorMessage("ERROR: No such host.");
    }


	printf("Creating socket ...\n");
    connectSocket = socket(server->ai_family, server->ai_socktype, server->ai_protocol);
    if (connectSocket < 0) {
        ErrorMessage("ERROR: Unable to open socket.");
    }


	printf("Connecting to server ...\n");
    status = connect(connectSocket, server->ai_addr, server->ai_addrlen);
    if (status < 0) {
	ErrorMessage("ERROR: Unable to connect.");
    }


    freeaddrinfo(server);
}





void Client::ClientSend(char* sendMessage) {

#ifdef DEBUG
	printf("Sending stream of measurements ...\n");
#endif


    for (unsigned int i = 0; i < strlen(sendMessage); i++) {
		memset(sendBuffer, 0, DEFAULT_BUFFER_LENGTH);
    	memcpy(&sendBuffer, sendMessage + i , 1);
    	msgerror = send(connectSocket, sendBuffer, strlen(sendBuffer), 0);
		if (msgerror < 0) {
         	ErrorMessage("ERROR: Unable to write to socket");
		}
    }
}





void Client::ClientClose() {

	printf("Shutting down ...\n");
    close(connectSocket);
}






