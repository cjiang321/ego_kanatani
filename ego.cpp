#include "ego.h"

#include <iostream>
#include <eigen3/Eigen/Core>
#include <eigen3/unsupported/Eigen/MatrixFunctions>
#include <opencv2/core/eigen.hpp>
#include <opencv2/core/core.hpp>


cv::Mat_<float> MinEigen(cv::Mat_<float> &A)
{
    cv::Mat_<float> A_ = cv::Mat_<float>(A);
    cv::Mat_<float> eival, eivec, minv;

    cv::eigen(A_, true, eival, eivec);

#ifdef EGO_DEBUG
    std::cout << eival << std::endl;
    std::cout << eivec << std::endl;
#endif

    minv = eivec.row(eivec.rows - 1).t();

    return minv;
}

ego::ego(std::vector< cv::Point2f > &inflow,
         std::vector< cv::Point2f > &inpts,
         float infl)
{
    size_t sz;

    if (inflow.size() == inpts.size())
    {
        sz = inflow.size();
    }
    else
    {
        std::cout << "WARNING::size of input vectors mismatch!" << std::endl;
        assert(0);
    }

    for (size_t i = 0; i < sz; i ++)
    {
        flow.push_back(inflow[i]);
        xp.push_back(inpts[i].x);
        yp.push_back(inpts[i].y);
    }

    focalLength = infl / infl;
}

int ego::kanatani(cv::Point3f &vtrans, cv::Point3f &vrot, float &depth_median)
{
    //compute n-vector, normalized to 1;
#ifdef EGO_DEBUG
    std::cout << "computing n-vector..." << std::endl;
#endif

    std::vector< cv::Point3f > m;   //n-vector
    std::vector< cv::Mat_<float> > _m;
    std::vector< float > v_norm;

    for (size_t i = 0; i < xp.size(); i++)
    {
        cv::Point3f pt(xp[i], yp[i], focalLength);                                          //temp point
        float _norm = cv::norm(pt);                                                         //norm of a single point
        v_norm.push_back(_norm);                                                            //store norm into array
        pt = cv::Point3f(xp[i]/_norm, yp[i]/_norm, focalLength/_norm);                      //normalise point
        m.push_back(pt);                                                                    //store point store in m in Point3f
        _m.push_back(cv::Mat_<float>(pt));                                                  //store point store in _m in Mat_<float>
    }

#ifdef EGO_DEBUG
    for (size_t i = 0; i < m.size(); i++)
        std::cout << m[i] << std::endl;

    //compute n-velocity
    std::cout << "computing n-velocity..." << std::endl;
#endif

    std::vector< cv::Point3f > m_dot;

    for (size_t i = 0; i < xp.size(); i++)
    {
        float tmp = 1 / v_norm[i];
        float tmp1 = (xp[i] * flow[i].x + yp[i] * flow[i].y) * (tmp*tmp*tmp);
        cv::Point3f pt(tmp * flow[i].x - tmp1 * xp[i],
                       tmp * flow[i].y - tmp1 * yp[i],
                       - focalLength * tmp1);
        m_dot.push_back(pt);
    }

#ifdef EGO_DEBUG
    for (size_t i = 0; i < m_dot.size(); i++)
        std::cout << m_dot[i] << std::endl;

    //compute twisted flow
    std::cout << "computing twisted flow..." << std::endl;
#endif

    std::vector< cv::Mat_<float> > m_dot_tw;

    for (size_t i = 0; i < xp.size(); i++)
    {
        cv::Point3f pt = m[i].cross(m_dot[i]);
        m_dot_tw.push_back(cv::Mat_<float>(pt));
    }

#ifdef EGO_DEBUG
    for (size_t i = 0; i < m_dot_tw.size(); i++)
        std::cout << m_dot_tw[i] << std::endl;

    //compute tensor L
    std::cout << "computing tensor L..." << std::endl;
#endif

    cv::Matx33f L(0, 0, 0,
                  0, 0, 0,
                  0, 0, 0);

    for (size_t k = 0; k < m_dot_tw.size(); k++)
    {
        cv::Mat_<float> pts_ = m_dot_tw[k];
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
            {
                L(i,j) += (pts_(i) * pts_(j));
            }
    }
#ifdef EGO_DEBUG
    std::cout << L << std::endl;

    //compute tensor M
    std::cout << "computing tensor M..." << std::endl;
#endif

    std::vector< float > M;

    for (int k = 0; k < 3; k++)
    {
        cv::Matx33f tmp(0, 0, 0,
                        0, 0, 0,
                        0, 0, 0);

        for (size_t l = 0; l < m.size(); l++)
        {
            cv::Mat_<float> pts_ = _m[l];
            for (int i = 0; i < 3; i++)
                for (int j = i; j < 3; j++)
                {
                    tmp(i,j) += (m_dot_tw[l](k) * pts_(i) * pts_(j));
                }
        }

        tmp(1,0) = tmp(0,1);
        tmp(2,0) = tmp(0,2);
        tmp(2,1) = tmp(1,2);

        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                M.push_back(tmp(i,j));
    }

#ifdef EGO_DEBUG
    for (size_t i = 0; i < M.size(); i++)
        std::cout << M[i] << std::endl;

    //compute tensor N
    std::cout << "computing tensor N..." << std::endl;
#endif

    cv::Matx<float, 6, 2> ind(0, 0,
                               0, 1,
                               1, 1,
                               0, 2,
                               1, 2,
                               2, 2);
    cv::Matx66f N;
    for (int i = 0; i < 6; i++)
        for (int j = 0; j < 6; j++)
            N(i,j) = 0;

    for (size_t k = 0; k < m.size(); k++)
    {
        for (int i = 0; i < 6; i++)
            for (int j = i; j < 6; j++)
            {
                int u = ind(i,0);
                int v = ind(i,1);
                int s = ind(j,0);
                int t = ind(j,1);
                N(i,j) += (_m[k](u) * _m[k](v) * _m[k](s) * _m[k](t));
            }
    }
    for (int i = 0; i < 6; i++)
        for (int j = 0; j < i; j++)
            N(i,j) = N(j,i);
    for (int i = 0; i < 6; i++)
    {
        N(i,1) *= 2;
        N(i,3) *= 2;
        N(i,4) *= 2;
    }
    cv::Matx66f N_inv = N.inv();

#ifdef EGO_DEBUG
    std::cout << N << std::endl;
    std::cout << N_inv << std::endl;

    //construct matrix A
    std::cout << "computing matrix A..." << std::endl;
#endif

    cv::Matx33f A(0, 0, 0,
                  0, 0, 0,
                  0, 0, 0);
    cv::Mat_<float> B;
    std::vector< int > e;

    for (int i = 0; i < 6; i++)
    {
        int tmp = 3 * ind(i,1) + ind(i,0);
        e.push_back(tmp);
    }

#ifdef EGO_DEBUG
    std::cout << "e array generated" << std::endl;
    for (size_t i = 0; i < e.size(); i++)
        std::cout << e[i] << std::endl;
#endif

    std::vector< float > _M;
    std::vector< float > _B;
    cv::Mat_<float> Mj;

    for (int i = 0; i < 3; i++)
        for (int j = i; j < 3; j++)
        {
            _M.clear();
            for (int k = j*9; k < (j+1)*9; k++)
                _M.push_back(M[k]);

            Mj.release();
            for (int k = 0; k < 6; k++)
                Mj.push_back(_M[e[k]]);

            B = cv::Mat_<float>(N_inv) * Mj;

            cv::Matx33f tmp(0, 0, 0,
                            0, 0, 0,
                            0, 0, 0);

            for (int k = 0; k < 6; k++)
                tmp(ind(k,0),ind(k,1)) = B(k);

            tmp(1,0) = tmp(0,1);
            tmp(2,0) = tmp(0,2);
            tmp(2,1) = tmp(1,2);

            _B.clear();
            for (int k = 0; k < 3; k++)
                for (int l = 0; l < 3; l++)
                    _B.push_back(tmp(l,k));

            _M.clear();
            for (int k = i*9; k < (i+1)*9; k++)
                _M.push_back(M[k]);
            for (int k = 0; k < 9; k++)
                A(i,j) += (_M[k] * _B[k]);
        }

    cv::subtract(L, A, A);
    A(1,0) = A(0,1);
    A(2,0) = A(0,2);
    A(2,1) = A(1,2);

#ifdef EGO_DEBUG
    std::cout << "A" << std::endl <<A << std::endl;
#endif

    //construct matrix B
    std::cout << "computing matrix B..." << std::endl;

    size_t n = xp.size();
    B.release();
    B = cv::Mat::eye(3, 3, CV_32F);

    cv::Mat_<float> m_(3, n);

    for (size_t i = 0; i < _m.size(); i++)
        _m[i].copyTo(m_.col(i));
    cv::Mat_<float> tmp1;

    tmp1 = m_ * m_.t();

    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            B(i,j) = n * B(i,j) - tmp1(i,j);

#ifdef EGO_DEBUG
    std::cout << B << std::endl;

    //solev eigenvalue problem Av=cBv
    std::cout << "solving for v..." << std::endl;
#endif

    Eigen::Matrix3d eigenB, eigenBsqrt;
    cv::cv2eigen(B, eigenB);
    Eigen::MatrixSquareRoot< Eigen::Matrix3d > sqrtm(eigenB);
    sqrtm.compute(eigenBsqrt);

    cv::Mat_<float> inv_sqrtB;
    cv::eigen2cv(eigenBsqrt, inv_sqrtB);
    inv_sqrtB = inv_sqrtB.inv();

    tmp1 = inv_sqrtB * cv::Mat_<float>(A);
    tmp1 = tmp1 * inv_sqrtB;

    cv::Mat_<float> _v = inv_sqrtB * MinEigen(tmp1);

    float nn = cv::norm(_v);
    _v = _v * (1 / nn);

    std::cout << "last v norm: " << nn << std::endl;

#ifdef EGO_DEBUG
    std::cout << _v << std::endl;
#endif

    v = _v.at<cv::Point3f>(0);

    //compute symmtric matrix K
    std::cout << "computing symmetric matrix K..." << std::endl;

    cv::Matx61f _K(0, 0, 0, 0, 0, 0);
    cv::Mat_<float> K_ = cv::Mat_<float>(_K);
    cv::Mat_<float> Mm(6,1);

    for (int k = 0; k < 3; k++)
    {
        _M.clear();
        for (int i = k*9; i < (k+1)*9; i++)
            _M.push_back(M[i]);
        Mm.release();
        for (int i = 0; i < 6; i++)
            Mm.push_back(_M[e[i]]);

        cv::Mat_<float> B_ = cv::Mat_<float>(N_inv) * Mm;

        cv::subtract(K_, _v(k) * B_, K_);
    }

    cv::Matx33f K;

    for (int i = 0; i < 6; i++)
        K(ind(i,0), ind(i,1)) = K_(i);

    K(1,0) = K(0,1);
    K(2,0) = K(0,2);
    K(2,1) = K(1,2);

#ifdef EGO_DEBUG
    std::cout << K << std::endl;
#endif

    //compute w
    std::cout << "solving for w..." << std::endl;

    cv::Mat_<float> _w;
    cv::Mat_<float> Kv = cv::Mat_<float>(K) * _v;

    float trace = K(0,0) + K(1,1) + K(2,2);
    cv::subtract((3 * _v.dot(Kv) + trace) * 0.5 * _v, 2*Kv, _w);

#ifdef EGO_DEBUG
    std::cout << _w << std::endl;
#endif

    w = _w.at<cv::Point3f>(0);

    //check if v=0
    float sumdiff = 0.0;
    for (size_t i = 0; i < m.size(); i++)
    {
        sumdiff += cv::norm(m_dot[i] - m[i].cross(w));
    }

    //std::cout << "sumdiff  " << sumdiff << std::endl;

    if (sumdiff < 0.1)
    {
        v = cv::Point3f(0, 0, 0);
        depth_median = 1;
    }
    else
    {
        //depth info
        std::vector< float > dep;
        for (size_t i = 0; i < m.size(); i++)
        {
            float a = 1 - (m[i].dot(v) * m[i].dot(v));
            float b = m[i].dot(w.cross(v)) - m_dot[i].dot(v);
            float c = a / b;

            //std::cout << i << "   " << xp[i] << " " << yp[i]
            //             << "   " << flow[i] << "   " << c << "   " << c*v_norm[i] << std::endl;
            dep.push_back(c*v_norm[i]);
        }

        std::sort(dep.begin(), dep.end());
        depth_median = dep[dep.size()/2];
        std::cout << std::endl << dep[dep.size()/2] << std::endl;
    }

    //output
    vtrans = v;
    vrot = w;
    return 0;
}
